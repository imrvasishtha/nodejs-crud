const UserModel = require("../models/user");
const helper = require("../helper/index");

// Create and Save a new user
exports.create = async (req, res) => {
  if (!req.body.email || !helper.validateEmail(req.body.email)) {
    res.status(400).send({ message: "email address is not valid!" });
  } else {
    const user = new UserModel({
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      phone: req.body.phone,
    });

    await user
      .save()
      .then((data) => {
        res.send({
          message: "User created successfully!!",
          user: data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while creating user",
        });
      });
  }
};

// Retrieve all users from the database.
exports.findAll = async (req, res) => {
  try {
    const user = await UserModel.find();
    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

// Find a single User with an id
exports.findOne = async (req, res) => {
  try {
    const user = await UserModel.findById(req.params.id);
    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

// Update a user by the id in the request
exports.update = async (req, res) => {
  if (
    !req.body ||
    (req.body.email ? !helper.validateEmail(req.body.email) : false)
  ) {
    res.status(400).send({
      message: "Not a valid data",
    });
  } else {
    const id = req.params.id;

    await UserModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
      .then((data) => {
        if (!data) {
          res.status(404).send({
            message: `User not found.`,
          });
        } else {
          res.send({ message: "User updated successfully." });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message,
        });
      });
  }
};

// Delete a user with the specified id in the request
exports.destroy = async (req, res) => {
  await UserModel.findByIdAndRemove(req.params.id)
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `User not found.`,
        });
      } else {
        res.send({
          message: "User deleted successfully!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};
