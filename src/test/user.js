process.env.NODE_ENV = "test";

let mongoose = require("mongoose");
let User = require("../models/user");

let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../../index");
let should = chai.should();

chai.use(chaiHttp);

describe("Users", () => {
  beforeEach(async () => {
    await User.findOneAndDelete({ email: "ramantest@gmail.com" });
  });
  describe("/GET user", () => {
    it("it should GET all the Users", (done) => {
      chai
        .request(server)
        .get("/user")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          done();
        });
    });
  });
  describe("/POST user", () => {
    it("it should not POST a user without email", (done) => {
      let user = {
        firstName: "Raman",
        lastName: "Sharma",
        phone: "9711190164",
      };
      chai
        .request(server)
        .post("/user")
        .send(user)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be.a("object");
          done();
        });
    });
    it("it should POST a user ", (done) => {
      let user = {
        email: "ramantest@gmail.com",
        firstName: "Raman",
        lastName: "Sharma",
        phone: "9711190164",
      };
      chai
        .request(server)
        .post("/user")
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have
            .property("message")
            .eql("User created successfully!!");
          res.body.user.should.have.property("firstName");
          res.body.user.should.have.property("lastName");
          res.body.user.should.have.property("phone");
          res.body.user.should.have.property("email");
          done();
        });
    });
  });
  describe("/GET/:id user", () => {
    it("it should GET a user by the given id", (done) => {
      let user = new User({
        email: "ramantest@gmail.com",
        firstName: "Raman",
        lastName: "Sharma",
        phone: "9711190164",
      });
      user.save().then((data) => {
        chai
          .request(server)
          .get("/user/" + data.id)
          .send(user)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("_id").eql(user.id);
            done();
          });
      });
    });
  });
  describe("/PATCH/:id user", () => {
    it("it should UPDATE a user given the id", (done) => {
      let user = new User({
        email: "ramantest@gmail.com",
        firstName: "Raman",
        lastName: "Sharma",
        phone: "9711190164",
      });
      user.save().then((data) => {
        chai
          .request(server)
          .patch("/user/" + data.id)
          .send({
            email: "ramantest@gmail.com",
            firstName: "Raman test",
            lastName: "Sharma",
            phone: "9711190164",
          })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("User updated successfully.");
            done();
          });
      });
    });
  });
  /*
   * Test the /DELETE/:id route
   */
  describe("/DELETE/:id user", () => {
    it("it should DELETE a user given the id", (done) => {
      let user = new User({
        email: "ramantest@gmail.com",
        firstName: "Raman",
        lastName: "Sharma",
        phone: "9711190164",
      });
      user.save().then((user) => {
        chai
          .request(server)
          .delete("/user/" + user.id)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("User deleted successfully!");
            done();
          });
      });
    });
  });
});
